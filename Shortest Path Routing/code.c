// CSCE 3530 - PA5
// Alex Bilozertchev

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Infinity definition
#define INFINITY 99999

int main(int argc, char *argv[]) {

	// Check for proper command line arguments.
	if (argc != 2) {
		printf("\nInvalid command line arguments.  Running syntax: \n./code <input_file>\n");
		printf("\nExample:\n./code router.txt\n\n");
		return 0;
	}

	// Variables for storing node information
	int vertices = 6; 
	int **edge_weight;
	int **res;
	int i, j, k, ii, jj;

	// Variables for source, destination, weight and token
	// Allocating memory to buffer
	size_t buffer_size = 100;
	char *buffer = malloc(buffer_size * sizeof(char));
	char *token;
	char *source;
	char *dest;
	char nodes[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};
	int weight;

	// Input file
	char *filename;

	// Read in input file from command line arguments.
	filename = argv[1];
	FILE *file = fopen(filename, "r");

	// Check for file existance
	if (NULL == file) {
		fprintf(stderr, "Cannot open file: %s\n", filename);
		return 0;
	}
		
	// Allocate memory to edges and results
	edge_weight = (int **)calloc(sizeof(int),  6);
	res = (int **)calloc(sizeof(int), 6);
	for (i = 0; i < 6; i++) {
		edge_weight[i] = (int *)calloc(sizeof(int),  6);
		res[i] = (int *)calloc(sizeof(int), 6);
	}
		
	// Destroy all node connections
	for (i = 0; i < vertices; i++) {
		for (j = 0; j < vertices; j++) {
				res[i][j] = INFINITY;
		}
	}

	// Tokenize the input file lines
	printf("\n");
	while(-1 != getline(&buffer, &buffer_size, file)) {
		
		// Grab single line and split it up
		token = strtok(buffer, " ");
		int count=0;
		while(token != NULL)  {
			count++;
			if (count==1) { source = token; }
			if (count==2) { dest = token; }
			if (count==3) { weight = atoi(token); }
			
			// Tokenize
			token = strtok(NULL, " ");
		}
		
		// Translation Map
		// First pass
		if (nodes[0] == '\0') {
			nodes[0] = source[0];
			nodes[1] = dest[0];
		}
		
		// Sequential passes
		else {
			
			int i;
			int found = 0;
			
			// Check for existance
			for (i=0; i < 6; i++) {
				if (nodes[i] == source[0]) {
					found = 1;
					break;
				}
			}
			
			// Add node to map
			if (found != 1) {
				for (i=0; i < 6; i++) {
					if (nodes[i] == '\0') {
						nodes[i] = source[0];
						break;
					}
				}
			}
			
			// Reset flag
			found = 0;
			
			// Check for existance
			for (i=0; i < 6; i++) {
				if (nodes[i] == dest[0]) {
					found = 1;
					break;
				}
			}
			
			// Add node to map
			if (found != 1) {
				for (i=0; i < 6; i++) {
					if (nodes[i] == '\0') {
						nodes[i] = dest[0];
						break;
					}
				}
			}
		}
		
		// Print status to user
		printf("Adding Edge: (weight = %d)\n", weight);
		
		// Translate source node integers to letters 
		if (source[0] == nodes[0]) { ii = 0; }
		else if (source[0] == nodes[1]) { ii = 1; }
		else if (source[0] == nodes[2]) { ii = 2; }
		else if (source[0] == nodes[3]) { ii = 3; }
		else if (source[0] == nodes[4]) { ii = 4; }
		else if (source[0] == nodes[5]) { ii = 5; }
		
		// Translate destination node integers to letters 
		if (dest[0] == nodes[0]) { jj = 0; }
		else if (dest[0] == nodes[1]) { jj = 1; }
		else if (dest[0] == nodes[2]) { jj = 2; }
		else if (dest[0] == nodes[3]) { jj = 3; }
		else if (dest[0] == nodes[4]) { jj = 4; }
		else if (dest[0] == nodes[5]) { jj = 5; }
		
		// Set the edge weights
		edge_weight[ii][jj] = weight;
		edge_weight[jj][ii] = weight;
		
		// If no connection, weight is infinite
		if (edge_weight[ii][jj] == 0) {
				res[ii][jj] = INFINITY;
				res[jj][ii] = INFINITY;
		} 
		
		// Otherwise, set path weights
		else {
				res[ii][jj] = edge_weight[ii][jj];
				res[jj][ii] = edge_weight[jj][ii];
		}
		
		// Clear out source and destination
		source = NULL;
		dest = NULL;
			
	}

	// Reset self to self
	for (i=0; i < vertices; i++) {
		res[i][i] = 0;
	}

	// Close the file and free buffer memory
	printf("\n");
	fclose(file);
	free(buffer);

	// Display the edge weight matrix
	printf("---------------------------------------------\n\n");
	printf("Edge weight matrix:\n\n");

	// Print column names
	printf("   %c  %c  %c  %c  %c  %c\n", nodes[0], nodes[1], nodes[2], nodes[3], nodes[4], nodes[5]);
	for (i = 0; i < vertices; i++) {
		// Print row names
		if (i == 0) { printf("%c", nodes[0]);}
		if (i == 1) { printf("%c", nodes[1]);}
		if (i == 2) { printf("%c", nodes[2]);}
		if (i == 3) { printf("%c", nodes[3]);}
		if (i == 4) { printf("%c", nodes[4]);}
		if (i == 5) { printf("%c", nodes[5]);}
		
		// Print edge weights
		for (j = 0; j < vertices; j++) {
				printf("%3d", edge_weight[i][j]);
		}
		printf("\n");
	}
		
	// Calculate the shortest paths using Djikstra's Algorithm
	for (i = 0; i < vertices; i++) {
		for (j = 0; j < vertices; j++) {
			for (k = 0; k < vertices; k++) {
				if (res[j][k] > res[j][i] + res[i][k]) {
					res[j][k] = res[j][i] + res[i][k];
				}							
			}
		}	
	}

	// Display the shortest path matrix
	printf("\n---------------------------------------------\n");
	printf("\nShortest path matrix:\n\n");

	// Print column names
	printf("   %c  %c  %c  %c  %c  %c\n", nodes[0], nodes[1], nodes[2], nodes[3], nodes[4], nodes[5]);
	for (i = 0; i < vertices; i++) {
		// Print row names
		if (i == 0) { printf("%c", nodes[0]);}
		if (i == 1) { printf("%c", nodes[1]);}
		if (i == 2) { printf("%c", nodes[2]);}
		if (i == 3) { printf("%c", nodes[3]);}
		if (i == 4) { printf("%c", nodes[4]);}
		if (i == 5) { printf("%c", nodes[5]);}
		
		// Print shortest paths
		for (j = 0; j < vertices; j++) {
				if (res[i][j] == INFINITY)
						printf("%3d", 0);
				else
						printf("%3d", res[i][j]);
		}
		printf("\n");
	}

	// Create the output file
	FILE *out;
	out = fopen("forwarding_tables.txt", "w");

	// Display the forwarding tables
	printf("\n---------------------------------------------\n\n");
	printf("Forwarding Tables:\n\n");
	fprintf(out, "Forwarding Tables:\n\n");	

	// Variables to hold counter, result value, and three 
	// display-out values
	int counter=0;
	int result=0;
	char one, two, three;

	// Calculate the route for each shortest path
	for(i=0; i<vertices; i++) {
		for(counter=0; counter < vertices; counter++) {
			// Skip route to itself
			if (counter==i) {continue;}
			// Is the route through node 0?
			if (edge_weight[i][0] != 0 && res[i][counter] == res[i][0] + res[0][counter]) {
				result = 0;
			}
			// Is the route through node 1?
			if (edge_weight[i][1] != 0 && res[i][counter] == res[i][1] + res[1][counter]) {
				result = 1;
			}
			// Is the route through node 2?
			if (edge_weight[i][2] != 0 && res[i][counter] == res[i][2] + res[2][counter]) {
				result = 2;
			}
			// Is the route through node 3?
			if (edge_weight[i][3] != 0 && res[i][counter] == res[i][3] + res[3][counter]) {
				result = 3;
			}
			// Is the route through node 4?
			if ((edge_weight[i][4] != 0) && res[i][counter] == res[i][4] + res[4][counter]) {
				result = 4;
			}
			// Is the route through node 5?
			if ((edge_weight[i][5] != 0) && res[i][counter] == res[i][5] + res[5][counter]) {			
				result = 5;
				// Check for better path
				if ((res[i][result-1] != 0) && (res[i][result] > res[i][result-1])) { result = result-1;}
			}
			
			// Translate paths from integers to letters
			if (counter == 0) { one = nodes[0];}
			else if (counter == 1) { one = nodes[1];}
			else if (counter == 2) { one = nodes[2];}
			else if (counter == 3) { one = nodes[3];}
			else if (counter == 4) { one = nodes[4];}
			else if (counter == 5) { one = nodes[5];}
			
			// Translate paths from integers to letters
			if (i == 0) { two = nodes[0];}
			else if (i == 1) { two = nodes[1];}
			else if (i == 2) { two = nodes[2];}
			else if (i == 3) { two = nodes[3];}
			else if (i == 4) { two = nodes[4];}
			else if (i == 5) { two = nodes[5];}
			
			// Translate paths from integers to letters
			if (result == 0) { three = nodes[0];}
			else if (result == 1) { three = nodes[1];}
			else if (result == 2) { three = nodes[2];}
			else if (result == 3) { three = nodes[3];}
			else if (result == 4) { three = nodes[4];}
			else if (result == 5) { three = nodes[5];}
			
			// Display the resulting forwarding route
			printf("%c (%c, %c)\n", one, two, three);
			fprintf(out, "%c (%c, %c)\n", one, two, three);
			
			// Clear out route variables
			one = '\0';
			two = '\0';
			three = '\0';

		}
		printf("\n");
		fprintf(out, "\n");
	}

	printf("Output file generated: forwarding_tables.txt\n\n");

	// Close the output file
	close(out);
	return 0;
}