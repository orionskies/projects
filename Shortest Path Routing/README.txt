CSCE 3530 - PA5 - README
Alex Bilozertchev

----------------------------------------------

Prerequisites:

	Include code.c, router.txt, and makefile
	in compilation directory.

	Best way is to simply unzip the archive,
	and execute from unzipped directory.

----------------------------------------------

Assumptions:

	Router.txt file contains the map between 
	6 routers with appropriate weights.

----------------------------------------------

Compilation: 

	gcc:
		gcc -o code code.c
	
	makefile:
		make

----------------------------------------------

Execution: 
	command line:
		./code router.txt
	
	makefile (run):
		make run

	makefile (clean):
		make clean

----------------------------------------------

Operation:

	This program supports 6 routers, as required in the instructions document.
	The code runs Djikstra's Algorithm on the input nodes in order to find the 
	shortest path between them.
	
	The weight and shortest path matrices are generated upon the execution of the
	program, and displayed to the user.  The shortest path matrix is generated
	using Djikstra's algorithm at runtime.
	
	Additionally, the forwarding tables for all six routers are then calculated 
	and displayed to the console, as well as saved to the forwarding_tables.txt
	output file.

	A code execution test case can be seen in the TEST_CASE.pdf document.
