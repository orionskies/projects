
CSCE 3530 - PA4 - Opening and Closing TCP Handshake + Sending File
Alex Bilozertchev

--------------------------------------------------------------------------

Running syntax:
    ./server <port>
    ./client <hostname> <port> <sendfile.txt>

--------------------------------------------------------------------------

Makefile commands:
    - make client - compilers client.c into client
    - make server - compilers server.c into server
    - make clean  - removes client, server, RECEIVED.txt client.out, and server.out

--------------------------------------------------------------------------

Basic Code Operation:
    The program is able to transfer the file of any size, not just limited to 1024 bytes.
    The server is executed on a specific port specified by the command line arguments.
    The received file is written out to RECEIVED.txt on the server.
    The server binds a socket to the port and waits for client connections.
    When the client runs, it opens its own socket and connects to the hostname and port specified
    by the command line arguments.
    The program creates a struct with all of the TCP header fields, and demonstrates the initiation and closing
    of a TCP handshake by modifying these fields during operation based on instructions.
    Additionally, the program transfers a 1024 byte text file from client to server using TCP.
    The length of the header is exactly 24 bytes as specified in the instructions.
    While the program is running, it also outputs the values of every packet sent to the console as well as the client.out
    and server.out files to be viewed later.
