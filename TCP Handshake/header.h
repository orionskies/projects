// CSCE 3530 - PA4 - Headers
// Alex Bilozertchev

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

// Global error function
void error(const char *msg) {
    perror(msg);
    exit(0);
}

// TCP Header
struct tcp_hdr {
	unsigned short int src : 16;			// source port
	unsigned short int des : 16;			// dest port
	unsigned int seq : 32;					// seq #
	unsigned int ack : 32;					// ack #
	unsigned int offset : 4;				// offset
	unsigned int reserved : 6;				// reserved
	unsigned short int hdr_flags : 6;		// flags
	unsigned short int rec : 16;			// window
	unsigned short int cksum : 16;			// checksum
	unsigned short int ptr : 16;			// urgent pointer
	unsigned int opt : 32;					// options
    char data[128];
};

// Print TCP contents and write to file
void print_tcp(struct tcp_hdr tcp_seg, FILE *fout) {
	// Print to console
	printf("source:\t\t0x%04X\n", tcp_seg.src);
	printf("dest:\t\t0x%04X\n", tcp_seg.des);
	printf("seq:\t\t0x%08X\n", tcp_seg.seq);
	printf("ack:\t\t0x%08X\n", tcp_seg.ack);
	printf("offset:\t\t0x%04X\n", tcp_seg.offset);
	printf("reserved:\t0x%04X\n", tcp_seg.reserved);
	printf("flags:\t\t0x%04X\n", tcp_seg.hdr_flags);
	printf("receive:\t0x%04X\n", tcp_seg.rec);
	printf("checksum:\t0x%04X\n", tcp_seg.cksum);
	printf("pointer:\t0x%04X\n", tcp_seg.ptr);
	printf("options:\t0x%08X\n", tcp_seg.opt);
	printf("\n");

	// Write to file
	fprintf(fout, "source:\t\t0x%04X\n", tcp_seg.src);
	fprintf(fout, "dest:\t\t0x%04X\n", tcp_seg.des);
	fprintf(fout, "seq:\t\t0x%08X\n", tcp_seg.seq);
	fprintf(fout, "ack:\t\t0x%08X\n", tcp_seg.ack);
	fprintf(fout, "offset:\t\t0x%04X\n", tcp_seg.offset);
	fprintf(fout, "reserved:\t0x%04X\n", tcp_seg.reserved);
	fprintf(fout, "flags:\t\t0x%04X\n", tcp_seg.hdr_flags);
	fprintf(fout, "receive:\t0x%04X\n", tcp_seg.rec);
	fprintf(fout, "checksum:\t0x%04X\n", tcp_seg.cksum);
	fprintf(fout, "pointer:\t0x%04X\n", tcp_seg.ptr);
	fprintf(fout, "options:\t0x%08X\n", tcp_seg.opt);
	fprintf(fout, "\n");
}

// Calculate Checksum
unsigned int checksum(unsigned short int cksum_arr[12]) {
	unsigned int i,sum=0, cksum;

	// Calculate sum
	for (i=0;i<12;i++) {
		sum = sum + cksum_arr[i];
	}

	cksum = sum >> 16;		// Fold once
	sum = sum & 0x0000FFFF;
	sum = cksum + sum;

	cksum = sum >> 16;		// Fold once more
	sum = sum & 0x0000FFFF;
	cksum = cksum + sum;

	// Return calculated checksum
	return cksum;
}
