// CSCE 3530 - PA4 - Server
// Alex Bilozertchev

#include "header.h"

int main(int argc, char *argv[]) {

	// Structs for the TCP header variables,
	// and variables used for socket connection
	// also holds the array to calculate checksum,
	// and socket file descriptors
	// temp is to hold the temp seq numbers
	struct tcp_hdr tcp_seg;
	struct sockaddr_in serv_addr, cli_addr;
	unsigned short int cksum_arr[12];
	unsigned int i, sum=0, cksum, temp;
	int sockfd, newsockfd, portno;
	int n;
	socklen_t clilen;

	// File for writing out
	FILE *fout;
	fout = fopen("server.out", "w+");

	// File for saving received payload
	FILE *received_file;
	received_file = fopen("RECEIVED.txt", "a");

	// Zero out the tcp segment
	bzero(&tcp_seg, sizeof(struct tcp_hdr));

	// Check for command line arguments
	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}

	// Define the socket descriptor
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {error("ERROR opening socket");}

	// Zero out address
	bzero((char *) &serv_addr, sizeof(serv_addr));

	// Get port number from command line arguments
	portno = atoi(argv[1]);

	// Socket setup
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	// Bind the socket
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {error("ERROR on binding");}

	// Start listening
	printf("listening...\n");
	listen(sockfd,5);
	clilen = sizeof(cli_addr);

	// Accept new connection
	newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
	if (newsockfd < 0)  {error("ERROR on accept");}

	// Read from connection
	n = read(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR reading from socket");}

	// Print out received data
	printf("RECEIVED SYN: \n");
	fprintf(fout, "RECEIVED SYN:\n");
	print_tcp(tcp_seg, fout);


	// Prepare TCP Packet
	// initial seq # of 210
	// ack # of 211
	// syn bit set to 1
	// ack bit set to 1
	tcp_seg.ack = tcp_seg.seq + 1;
	tcp_seg.seq = 210;
	tcp_seg.hdr_flags = 18;

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));

	// Print out received data
	printf("SENDING SYN-ACK.\n");
	fprintf(fout, "SENDING SYN-ACK:\n");
	print_tcp(tcp_seg, fout);

//open 2
	n = write(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR writing to socket");}

	n = read(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR reading from socket");}
	// Print out received data
	printf("RECEIVED ACK:\n");
	fprintf(fout, "RECEIVED ACK:\n");
	print_tcp(tcp_seg, fout);
	fprintf(received_file, "%s", tcp_seg.data);
	strcpy(tcp_seg.data, "");

	printf("3-way handshake successful.\n");
	fprintf(fout, "3-way handshake successful.\n");
	printf("\n----------------------------\n\n");
	fprintf(fout, "\n----------------------------\n\n");

	// RECEIVE PAYLOAD
	int ii;
	for(ii=0; ii<100; ii++) {
		// Read from socket
		n = read(newsockfd,&tcp_seg,sizeof(tcp_seg));
		if (n < 0) {error("ERROR reading from socket");}

		// Is the FIN bit detected?
		if (tcp_seg.hdr_flags != 1) {
			printf("RECEIVED PAYLOAD PACKET:\n");
			fprintf(fout, "RECEIVED PAYLOAD PACKET:\n");
			print_tcp(tcp_seg, fout);
			// write to file and reset
			fprintf(received_file, "%s", tcp_seg.data);

			// Clear data
			strcpy(tcp_seg.data, "");

			// Update ack and sequence numbers
			tcp_seg.ack = tcp_seg.seq + 1;
			tcp_seg.seq = tcp_seg.seq + 128;
			// Calculate checksum
			memcpy(cksum_arr, &tcp_seg, 24);
			tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));

			// Write back.
			printf("SENDING PAYLOAD ACK:\n");
			fprintf(fout, "SENDING PAYLOAD ACK:\n");
			print_tcp(tcp_seg, fout);
			n = write(newsockfd,&tcp_seg,sizeof(tcp_seg));
			if (n < 0) {error("ERROR writing to socket");}
		}
		else {
			break;
		}
	}
	fclose(received_file);

	// Closing connection
	printf("\n----------------------------\n");
	printf("\nClosing connection:\n\n");

	fprintf(fout, "\n----------------------------\n");
	fprintf(fout, "\nClosing connection:\n\n");
	// Print out received data
	printf("RECEIVED FIN:\n");
	fprintf(fout, "RECEIVED FIN:\n");
	print_tcp(tcp_seg, fout);

	// Prepare TCP Packet
	// seq # = initial seq # 220
	// ack # = initial seq # + 1
	// ack bit set to 1
	tcp_seg.ack = tcp_seg.seq + 1;
	tcp_seg.seq = 220;
	tcp_seg.hdr_flags = 16;	// setting the ACK bit to 1

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));
	// Print out received data
	printf("SENDING FIN-ACK.\n");
	fprintf(fout, "SENDING FIN-ACK:\n");
	print_tcp(tcp_seg, fout);

//close 2
	n = write(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR writing to socket");}

	// Prepare TCP Packet
	// set FIN bit to 1
	tcp_seg.hdr_flags = 1;	// setting the FIN bit to 1

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));
	// Print out received data
	printf("SENDING FIN.\n");
	fprintf(fout, "SENDING FIN:\n");
	print_tcp(tcp_seg, fout);

//close 3
	n = write(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR writing to socket");}

	n = read(newsockfd,&tcp_seg,sizeof(tcp_seg));
	if (n < 0) {error("ERROR reading from socket");}
	// Print out received data
	printf("RECEIVED FIN-ACK: \n");
	fprintf(fout, "RECEIVED FIN-ACK:\n");
	print_tcp(tcp_seg, fout);

	printf("WRITING OUT PAYLOAD TO RECEIVED.txt... DONE.\n\n");

	// close file descriptors and file
	close(newsockfd);
	close(sockfd);
	fclose(fout);
    return 0;
}
