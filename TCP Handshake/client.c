// CSCE 3530 - PA4 - Client
// Alex Bilozertchev

#include "header.h"

int main(int argc, char *argv[]) {

	// Structs for the TCP header variables,
	// and variables used for socket connection
	// also holds the array to calculate checksum,
	// and socket file descriptors
	// temp is to hold the temp seq numbers
	struct tcp_hdr tcp_seg;
	struct sockaddr_in serv_addr;
    struct hostent *server;
	unsigned short int cksum_arr[12];
	int sockfd, portno, n;
	unsigned int temp;

	// Variables to hold the input file to transfer
	long fsend_size;
	char *file_content;

	// File for writing out
	FILE *fout;
	fout = fopen("client.out", "w+");

	// Check for command line arguments.
	if (argc != 4) {
       fprintf(stderr,"usage %s <hostname> <port> <file>\n", argv[0]);
       exit(0);
    }

	FILE *fsend = fopen(argv[3], "rb" );

	if ( fsend == 0 ) {
		printf( "Could not open file\n" );
	}

	else {
		// Read in contents of file
		fseek(fsend, 0, SEEK_END);
		fsend_size = ftell(fsend);
		rewind(fsend);
		file_content = malloc(fsend_size * (sizeof(char)));
		fread(file_content, sizeof(char), fsend_size, fsend);
		fclose(fsend);
	}

	// Get port number
    portno = atoi(argv[2]);

	// Generate the initial TCP header packet
	// initial seq # of 100
	// initial ack # of 0
	// syn bit set to 1
	tcp_seg.src = atoi(argv[2]);
	tcp_seg.des = atoi(argv[2]);
	tcp_seg.seq = 100;
	tcp_seg.ack = 0;
	tcp_seg.offset = 6;
	tcp_seg.reserved = 0;
	tcp_seg.hdr_flags = 2;
	tcp_seg.rec = 0;
	tcp_seg.cksum = 0;
	tcp_seg.ptr = 0;
	tcp_seg.opt = 0;
	strcpy(tcp_seg.data, "");
	tcp_seg.data[128] = 0;

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));

	printf("SENDING SYN:\n");
	fprintf(fout, "SENDING SYN:\n");
	print_tcp(tcp_seg, fout);

	// Create file descriptor for socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {error("ERROR opening socket");}

	// Get hostname
	server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }


	// Socket related variables
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

	// Connect on socket
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {error("ERROR connecting");}
//open 1
	// Write to socket
    n = write(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR writing to socket");}


	// Read from socket
    n = read(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR reading from socket");}

	// Print out received data
	printf("RECEIVED SYN-ACK: \n");
	fprintf(fout, "RECEIVED SYN-ACK:\n");
	print_tcp(tcp_seg, fout);

	// Prepare TCP Packet
	// seq # = initial seq # + 1
	// ack # = initial seq # + 1
	// ack bit set to 1
	temp = tcp_seg.seq;
	tcp_seg.seq = tcp_seg.ack;
	tcp_seg.ack = temp + 1;
	tcp_seg.hdr_flags = 16;

	// Send data payload
	strncpy(tcp_seg.data, file_content, 128);
	if (strlen(file_content) > 128) {
		file_content += 128;
	}

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));

	// Print out received data
	printf("SENDING ACK + PAYLOAD PACKET #1:\n");
	fprintf(fout, "SENDING ACK + PAYLOAD PACKET #1:\n");
	print_tcp(tcp_seg, fout);

//open 3
	n = write(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR writing to socket");}

	printf("3-way handshake successful.\n");
	printf("\n----------------------------\n\n");
	fprintf(fout, "3-way handshake successful.\n");
	fprintf(fout, "\n----------------------------\n\n");


	// SEND PAYLOAD
	int i = 0;
	int ii = strlen(file_content);

	for (i = 0; i < ii; i++) {
		// copy part from file_content
		strncpy(tcp_seg.data, file_content, 128);

		// Update sequence and ack numbers
		temp = tcp_seg.seq;
		tcp_seg.seq = tcp_seg.ack;
		tcp_seg.ack = temp + 128;

		// Calculate new checksum
		memcpy(cksum_arr, &tcp_seg, 24);
		tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));

		// Write to server
		printf("SENDING PAYLOAD PACKET #%d:\n",i+2);
		fprintf(fout, "SENDING PAYLOAD PACKET #%d:\n",i+2);
		print_tcp(tcp_seg, fout);

		n = write(sockfd,&tcp_seg,sizeof(tcp_seg));
	    if (n < 0) {error("ERROR writing to socket");}

		// Read from server
		n = read(sockfd,&tcp_seg,sizeof(tcp_seg));
	    if (n < 0) {error("ERROR reading from socket");}
		// Print out received data
		printf("RECEIVED PAYLOAD ACK: \n");
		fprintf(fout, "RECEIVED PAYLOAD ACK: \n");
		print_tcp(tcp_seg, fout);

		// Strip off 128 char from file_content,
		// to use for next payload.
		if (strlen(file_content) > 128) {
			file_content += 128;
		}
		else {
			break;
		}
		printf("\n");
	}



	printf("\n----------------------------\n");
	fprintf(fout, "\n----------------------------\n");
	printf("\nClosing connection:\n\n");
	fprintf(fout, "\nClosing connection:\n\n");


	// Prepare TCP Packet
	// initial seq # 200
	// initial ack # 0
	// FIN bit set to 1
	tcp_seg.seq = 200;
	tcp_seg.ack = 0;
	tcp_seg.hdr_flags = 1;	// Setting FIN bit to 1

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));
	// Print out received data
	printf("SENDING FIN.\n");
	fprintf(fout, "SENDING FIN:\n");
	print_tcp(tcp_seg, fout);

// close 1
	n = write(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR writing to socket");}


    n = read(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR reading from socket");}
	// Print out received data
	printf("RECEIVED FIN-ACK: \n");
	fprintf(fout, "RECEIVED FIN-ACK:\n");
	print_tcp(tcp_seg, fout);


    n = read(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR reading from socket");}
	// Print out received data
	printf("RECEIVED FIN: \n");
	fprintf(fout, "RECEIVED FIN:\n");
	print_tcp(tcp_seg, fout);

	// Prepare TCP Packet
	// seq # = initial cli seq # + 1
	// ack # = initial svr seq # + 1
	// ack bit set to 1
	temp = tcp_seg.seq;
	tcp_seg.seq = tcp_seg.ack;
	tcp_seg.ack = temp + 1;
	tcp_seg.hdr_flags = 16;	// ACK BIT = 1

	// Calculate checksum
	memcpy(cksum_arr, &tcp_seg, 24);
	tcp_seg.cksum = (0xFFFF^checksum(cksum_arr));
	// Print out received data
	printf("SENDING FIN-ACK.\n");
	fprintf(fout, "SENDING FIN-ACK:\n");
	print_tcp(tcp_seg, fout);
//close 4
	n = write(sockfd,&tcp_seg,sizeof(tcp_seg));
    if (n < 0) {error("ERROR writing to socket");}

    close(sockfd);
	fclose(fout);
	return 0;
}
