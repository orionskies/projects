#include <iostream>
#include <fstream>
#include <cstdlib>
#include <locale>

using namespace std;

int main(int argc, char* argv[])
{
	ifstream file;
	char word[5];
	
    locale loc("");
    cout.imbue(loc);

	file.open("data.txt");
	if (!file.is_open()) {
		exit(EXIT_FAILURE);
	}

    if (argc < 4) { cout << "usage: bot <start> <end> <streak>\n"; return 0;}
	
	long int counter=0, odd=0, even=0, zeros=0, lines=atoi(argv[2]);
	int odd_streak=0, even_streak=0, highest_odd_streak=0, highest_even_streak=0;
    int odd_streak_end = 0, even_streak_end = 0, even_streak_count = 0, odd_streak_count = 0;
	while (counter < lines && file.good() ) {

        file.getline(word, 5);
		counter++;

        if (counter >= atoi(argv[1])) {
            cout << counter << ":\t";
            if (atoi(word) == 0 || atoi(word) == 00) {
                cout << "zero\t";
                zeros++;
                if (highest_odd_streak < odd_streak) {
                    highest_odd_streak = odd_streak;
                    odd_streak_end = counter;
                }
                if (highest_even_streak < even_streak) {
                    highest_even_streak = even_streak;
                    even_streak_end = counter;
                }
                if (even_streak >= atoi(argv[3])) {even_streak_count++;}
                if (odd_streak >= atoi(argv[3])) {odd_streak_count++;}
                even_streak = 0;
                odd_streak = 0;
            }
            else if ((atoi(word) %2 == 0)) {
                cout << "even\t";
                even++;
                even_streak++;
                if (highest_odd_streak < odd_streak) {
                    highest_odd_streak = odd_streak;
                    odd_streak_end = counter;
                }
                if (odd_streak >= atoi(argv[3])) {odd_streak_count++;}
                odd_streak = 0;
            }
            else {
                cout << "odd\t";
                odd++;
                odd_streak++;
                if (highest_even_streak < even_streak) {
                    highest_even_streak = even_streak;
                    even_streak_end = counter;
                }
                if (even_streak >= atoi(argv[3])) {even_streak_count++;}
                even_streak = 0;
            }
            cout << word << endl;
        }
	}

    // RUN ANOTHER ZERO TO CALIBRATE
    if (highest_odd_streak < odd_streak) {
        highest_odd_streak = odd_streak;
        odd_streak_end = counter;
    }
    if (highest_even_streak < even_streak) {
        highest_even_streak = even_streak;
        even_streak_end = counter;
    }
    if (even_streak >= atoi(argv[3])) {even_streak_count++;}
    if (odd_streak >= atoi(argv[3])) {odd_streak_count++;}
    even_streak = 0;
    odd_streak = 0;
	
    cout << "-----------------------" << endl;
	cout << "Analysis on " << counter - atoi(argv[1]) << " entries:" << endl << endl;
    cout << "Evens:\t" << even << "\t\tStreak: " << highest_even_streak << ", line " << even_streak_end - highest_even_streak;
    cout << "\t\tStreaks >= " << atoi(argv[3]) << ": " << even_streak_count << endl;
    cout << "Odds:\t" << odd << "\t\tStreak: " << highest_odd_streak << ", line " << odd_streak_end - highest_odd_streak;
    cout << "\t\tStreaks >= " << atoi(argv[3]) << ": " << odd_streak_count << endl;
    cout << "Zeros:\t" << zeros << endl << endl;
	
	return 0;
	
}