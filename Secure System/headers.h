#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

// Define standard namespace
using namespace std;

// Instructions class
class instruction {
    public:
    string type;
    string subjectName;
    string objectName;
    int value;
};

// Reference monitor class
class refmonitor {
    public:
    class subject {
        public:
        string name;
        int temp;
    };

    class object {
        public:
        string name;
        int value;
    };

    class level {
        public:
        string name;
        int level;
    };

    vector<subject> subjects;       // Holds the subject objects
    vector<object> objects;         // Holds the object objects
    vector<level> subject_levels;   // Holds the subject security levels
    vector<level> object_levels;    // Holds the object security levels

    // Adds a subject to the security system
    void createSubject(string name, string value) {
        
        // Create temporary subject and level
        subject temp;
        level lvl;
        
        // Set initial data
        temp.name = name;
        temp.temp = 0;
        lvl.name = name;
        
        // Set security level
        if (value == "LOW") { lvl.level = 1; }
        if (value == "MEDIUM") { lvl.level = 2; }
        if (value == "HIGH") { lvl.level = 3; }
        
        // Add subject to subjects vector
        subjects.push_back(temp);

        // Add subject security level to subject security vector
        subject_levels.push_back(lvl);
    }

    // Adds an object to the security system
    void createObject(string name, string value) {
        
        // Create temporary subject and level
        object temp;
        level lvl;
        
        // Set initial data
        temp.name = name;
        temp.value = 0;
        lvl.name = name;

        // Set security level
        if (value == "LOW") { lvl.level = 1; }
        if (value == "MEDIUM") { lvl.level = 2; }
        if (value == "HIGH") { lvl.level = 3; }

        // Add object to objects vector
        objects.push_back(temp);

        // Add object security level to object security vector
        object_levels.push_back(lvl);
    }

    // Process the read command
    int read(instruction instruct) {
        
        // Variables for count and initial data
        int i;
        int subject_location = 0;
        int object_location = 0;
        int subject_level = 0;
        int object_level = 0;
        if (exists(instruct)) {
            // Find vector location of subject and object
            for (i=0; i < 6; i++) {
                if (instruct.subjectName == subjects[i].name) {
                    subject_location = i;
                }
                if (instruct.objectName == objects[i].name) {
                    object_location = i;
                }
            }
            // Find security level
            for (i=0; i < 6; i++) {
                if (subjects[subject_location].name == subject_levels[i].name) {
                    subject_level = subject_levels[i].level;
                }
                if (objects[object_location].name == object_levels[i].name) {
                    object_level = object_levels[i].level;
                }
            }
            // Access granted
            if (subject_level >= object_level) {
                // Perform read
                subjects[subject_location].temp = objects[object_location].value;
                return 1;
            }
            // Access denied
            else { return 2; }
        }
        // Bad instruction
        else { return 0; }
    }

    // Process the write command
    int write(instruction instruct) {
        
         // Variables for count and initial data
        int i;
        int subject_location = 0;
        int object_location = 0;
        int subject_level = 0;
        int object_level = 0;
        if (exists(instruct)) {
            // Find vector location of subject and object
            for (i=0; i < 6; i++) {
                if (instruct.subjectName == subjects[i].name) {
                    subject_location = i;
                }
                if (instruct.objectName == objects[i].name) {
                    object_location = i;
                }
            }
            // Find security level
            for (i=0; i < 6; i++) {
                if (subjects[subject_location].name == subject_levels[i].name) {
                    subject_level = subject_levels[i].level;
                }
                if (objects[object_location].name == object_levels[i].name) {
                    object_level = object_levels[i].level;
                }
            }
            // Access granted
            if (subject_level <= object_level) {
                
                // Perform write
                objects[object_location].value = instruct.value;
                return 1;
            }
            // Access denied
            else { return 2; }
        }
        // Bad instruction
        else { return 0; }
    }

    // Print the state of all subject and objects and their values
    void printState() {
        
        // Counte variable
        int i;
        
        // Print out the subject name
        cout << "+-------------------------------The current state is--------------------------------+" << endl;
        cout << setw(13) << "| subject: | ";
        for (i=0; i < 6; i++) {
            cout << setw(10) << subjects[i].name << " |";
        }

        // Print out the subject value
        cout << endl << setw(13) << "| value:   | ";
        for (i=0; i < 6; i++) {
            cout << setw(10) << subjects[i].temp << " |";
        }

        // Print out the object name
        cout << endl << "+-----------------------------------------------------------------------------------+" << endl;
        cout << setw(13) << "| object:  | ";
        for (i=0; i < 6; i++) {
            cout << setw(10) << objects[i].name << " |";
        }

        // Print out the object value
        cout << endl << setw(13) << "| value:   | ";
        for (i=0; i < 6; i++) {
            cout << setw(10) << objects[i].value << " |";
        }
        cout << endl << "+-----------------------------------------------------------------------------------+" << endl;    
    }

    // Used to check if the subjects and/or objects exist
    bool exists(instruction instruct) {
        
        // Counter and initial values
        int i;
        int subject_status = 0;
        int object_status = 0;

        // Subject is found
        for (i=0; i < 6; i++) {
            if (instruct.subjectName == subjects[i].name) {
                subject_status = 1;
            }
        }

        // Object is found
        for (i=0; i < 6; i++) {
            if (instruct.objectName == objects[i].name) {
                object_status = 1;
            }
        }

        // The subject and object exist
        if (subject_status == 1 && object_status == 1) {
            return true;
        }

        // One of them does not exist
        else { return false; }
    }
};

// Used to display bad instruction error message
void bad(string line) {
    cout << "Bad Instruction\t: " << line << endl;
}