// CSCE 4550 - Project 1
// Alex Bilozertchev

// Include classes, functions, and headers.
#include "headers.h"

int main(int argc, char* argv[]) { 

    // Variables for input string, ref monitor object, and instructions object
    string line;
    refmonitor RefMon;
    ifstream instructions;
    
    // Check for command line arguments
    if (argc < 2) {
        cout << "Incorrect command line arguments." << endl << "Usage: ./securitysystem <instruction_file>" << endl;
        return(0);
    }

    // Open the instructions file
    else {
        instructions.open (argv[1], ifstream::in);
        // Instructions file does not exist
        if (!instructions) {
            cout << "Could not open instructions file." << endl;
            return(0);
        }
    }

    // Create subjects
    RefMon.createSubject("adam", "LOW");
    RefMon.createSubject("james", "MEDIUM");
    RefMon.createSubject("tim", "HIGH");
    RefMon.createSubject("sara", "LOW");
    RefMon.createSubject("kristy", "MEDIUM");
    RefMon.createSubject("liz", "HIGH");

    // Create objects
    RefMon.createObject("aobj", "LOW");
    RefMon.createObject("jobj", "MEDIUM");
    RefMon.createObject("tobj", "HIGH");
    RefMon.createObject("sobj", "LOW");
    RefMon.createObject("kobj", "MEDIUM");
    RefMon.createObject("lobj", "HIGH");

    // Process instructions list
    while (getline(instructions, line)) {
        istringstream iss(line);        // Put line string into stringstream
        string one, two, three, five;   // Strings to hold commands
        int four;                       // Integer to hold value command
        int status = 0;                 // Status variables used to indicate bad instructions

        // Check first value in string stream
        if ((iss >> one)) {
            // The command is 'read'
            if (one == "read" || one == "READ") {
                // Check the next two arguments
                if ((iss >> two >> three)) {
                    // If there is no fourth argument
                    if (!(iss >> four)) {
                        // Create and populate instructions object
                        instruction instruct;
                        instruct.type = one;
                        instruct.subjectName = two;
                        instruct.objectName = three;
                        instruct.value = 0;
                        // Set good command status
                        status = RefMon.read(instruct);
                        // Print out state
                        if (status == 1) {
                            cout << "Access Granted\t: " << two << " reads " << three << endl;
                            RefMon.printState();
                        }
                        // Print out error message
                        else if (status == 2) {
                            cout << "Access Denied\t: " << line << endl;
                        }
                    }  
                }
            }
            // The command is 'write'
            else if (one == "write" || one == "WRITE") {
                // Check the next three commands
                if ((iss >> two >> three >> four)) {
                    // If there is no fifth argument
                    if (!(iss >> five)) {
                        // Create and populate instructions object
                        instruction instruct;
                        instruct.type = one;
                        instruct.subjectName = two;
                        instruct.objectName = three;
                        instruct.value = four;
                        // Set good command status
                        status = RefMon.write(instruct);
                        // Print out state
                        if (status == 1) {
                            cout << "Access Granted\t: " << two << " writes value " << four << " to " << three << endl;
                            RefMon.printState();
                        }
                        // Print our error message
                        else if (status == 2) {
                            cout << "Access Denied\t: " << line << endl;
                        }
                    }
                }
            }
        } 

        // Invalid command message
        if (status == 0) { bad(line); }
    }
    
    // End execution
    cout << endl;
    return(0);
}
