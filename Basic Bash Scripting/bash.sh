#!/bin/bash
# Basic Bash Scripting
# CSCE 3600
# Alexander Bilozertchev


if [ "$#" -eq 0 ]   # check if command-line arguments are supplied
then
    echo "usage: minor3 name [euid]"    # if not, display requirements
else    # introductory messages
    echo "Good day, $1!  Nice to meet you!"     # greet user by username supplied
    echo "-------------------------------------------------------------------|"
    echo "Enter one of the following options:                                |"
    echo "1) List and count all non-hidden files in the current directory.   |"
    echo "2) Check if given user (default = current user) is logged in,      |"
    echo "   then list all active processes for that user.                   |"
    echo "3) List the sizes and names of the 10 largest files and            |"
    echo "   directories in the current directory.                           |"
    echo "4) Exit the shell program.                                         |"
    echo "-------------------------------------------------------------------|"

    read -p "> " choice     # read in the user's choice
    if [ "$choice" -eq 1 ]  # choice is 1
    then
        echo -n "Total number of files: "
        ls | wc -l  # list total number of files
        ls  # list the actual files
    elif [ "$choice" -eq 2 ]    # choice is 2
    then
        if [ -n "$2" ]  # Check if ID is provided
        then
            echo "Active processes for $2:" # ID is provided
            ps -u $2    # display active processes for ID.  Logged in, or otherwise, does not exist error.
        else
            echo "Active processes for $USER (logged in):"  # ID NOT provided.  Using default.
            ps -u $USER # display active processes for default ID.  Always logged in.
        fi
    elif [ "$choice" -eq 3 ]    # choice is 3
    then
        echo "Size and Name of 10 largest files and directories:"
        du -a --max-depth=1 | sed s@\./@@g | sort -n -r | head -n 10    # list largest files/directories.  Remove the dot-slash.
    else
        echo "Thanks, $1!  Have a great day!"   # Goodbye message
    fi
fi
