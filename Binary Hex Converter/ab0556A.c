//
// ASCII / Decimal / HEX / Binary Converter
// Author: Alex Bilozertchev
// 


// Libraries
#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    // Variables for the ascii character and unsigned int for conversion
    char c;
    unsigned int x;
    
    // Output to user
    printf("Enter an ASCII character: ");
    
    // Input ascii character
    scanf("%c",&c);
    
    // Cast character as unsigned int
    x = (int)c;
    
    // Output values
    printf("The ASCII value of %c is: %d (dec) -- %x (hex) -- ",c,x,x);
    
    // Binary-generating loop
    while (x) {
        if (x & 1) {
            printf("1");
        }
        else {
            printf("0");
        }
        x >>= 1;
    }
    
    // Additional output
    printf(" (bin)\n\n");
    
    // Return code to system
    return 0;
}
