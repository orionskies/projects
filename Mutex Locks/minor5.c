
// Implementing Mutex Locks
// CSCE 3600
// Alexander Bilozertchev

// Libraries
#include <stdio.h>
#include <string.h>
#include <pthread.h>

// Character limit
#define SIZE 50

// Variables to hold the sentence, and an index variable
char sentence[2000];
int  ind = 0;

// Initialize mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

char convertUppercase(char lower)
{
	// Converts lowercase to uppercase
	if ((lower > 96) && (lower < 123))
	{
		return (lower - 32);
	}
	else
	{
		return lower;
	}
}

void printChar()
{
	// Prints the converted sentence
	printf("The new sentence is [%d]: \t%c\n", ind, sentence[ind]);
	ind++;
}

void *convertMessage(void *ptr)
{
	// Start Critical Section - Shared data

	// Lock data access
	pthread_mutex_lock(&mutex);
		// Function that each threads initiates its execution
		char aux;

		// Check if odd or even index
		if (ind % 2)
		{
			sentence[ind] = convertUppercase(sentence[ind]);
		}

		printChar();
	// Unlock data access
	pthread_mutex_unlock(&mutex);
	// End Critical Section
	return 0;
}

int main()
{
	int i;
	char buffer[SIZE];
	char *p;
	pthread_t ts[SIZE]; // define up to 50 threads

	printf("Please enter a phrase (less than 50 characters): ");

	if (fgets(buffer, sizeof(buffer), stdin) != NULL)
	{
		if ((p = strchr(buffer, '\n')) != NULL)
		{
			*p = '\0';
		}
	}

	strcpy(sentence, buffer); // copy string to char array

	printf("The original sentence is: \t %s\n", sentence);

	// Create one thread for each character on the input word
	for(i = 0; i < strlen(buffer) + 1; ++i)
	{
		pthread_create(&ts[i], NULL, convertMessage, NULL);
	}

	// We wait until all threads finish execution
	for(i = 0; i < strlen(buffer); i++)
	{
		pthread_join(ts[i], NULL);
	}

	// newline character
	printf("\n");

	return 0;
}
